#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>

//Untuk menghapus ".jpg" pada getUmur
char* cut_four (char* s) {
    int n;
    int i;
    char* new;
    for (i = 0; s[i] != '\0'; i++);
    // lenght of the new string
    n = i - 4 + 1;
    if (n < 1)
        return NULL;
    new = (char*) malloc (n * sizeof(char));
    for (i = 0; i < n - 1; i++)
        new[i] = s[i];
    new[i] = '\0';
    return new;
}

// Menjalankan copy 
void copy(char *src, char *dest) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"cp", "-n", src, dest, NULL};
		execv("/usr/bin/cp", argv);
	} else {
		((wait(&status)) > 0);
	}
}

// Menjalankan delete
void delete (char *file) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"rm", "-d", file, NULL};
		execv("/usr/bin/rm", argv);
	} else {
		((wait(&status)) > 0);
	}
}

// Fungsi makedir
void makedir(char *dest) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		printf("%s\n", dest);
		char *argv[] = {"mkdir", "-p", dest, NULL};
		execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void listFilesRecursively(char *basePath) {
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
		return;

	while ((dp = readdir(dir)) != NULL) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			char tempFolder[100];
			char mkFolder[100] = "shift2/drakor/";
			char kat1[100] = "shift2/drakor/";
			char kat2[1000] = "shift2/drakor/";
			char kat3[1000] = "shift/drakor/";
			char temp[1000], temp2[1000], temp3[1000], temp4[1000], getNama[1000], getTahun[1000], getKat[100], getNama2[1000], getTahun2[1000], getKat2[100], getNama3[1000], getTahun3[1000], getKat3[100];
			char *token, *token2, *token3;

			//strcpy(tempFolder, dp->d_name);
			//printf("%s\n", dp->d_name);
			//strtok(tempFolder, ";");
			//strcat(mkFolder, tempFolder);

			char listFile[100] = "shift2/drakor/";
			strcat(listFile, dp->d_name);

			if (!(strstr(dp->d_name, "_"))) {
				strcpy(temp, dp->d_name); 
				token = strtok(temp, ";"); 
				strcpy(getNama, token);
				token = strtok(NULL, ";"); 
				strcpy(getTahun, token);
				token = strtok(NULL, ";"); 
				strcpy(getKat, token);
				strcat(mkFolder, getKat);
				//printf("1 : %s %s\n", getNama, getKat);
				strcat(kat1, getKat);
				strcat(kat1, "/");
				strcat(kat1, getNama);
				strcat(kat1, ".png");
			}
			
			if (strstr(dp->d_name, "_")) {
				strcpy(temp3, dp->d_name); 
				token3 = strtok(temp3, ";"); 
				strcpy(getNama2, token3);
				token3 = strtok(NULL, ";");
				strcpy(getTahun2, token3);
				token3 = strtok(NULL, ";"); 
				strcpy(getKat2, token3);
				//strcat(mkFolder, getKat2);
				//printf("2 : %s %s\n", getNama2, getKat2);
				strcat(kat2, getKat2);
				strcat(kat2, "/");
				strcat(kat2, getNama2);
				strcat(kat2, ".png");
				
				strcpy(temp2, dp->d_name); 
				token2 = strtok(temp2, "_"); 
				token2 = strtok(NULL, "_");
				strcpy(temp4, token2);
				token2 = strtok(temp4, ";");
				strcpy(getNama3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getTahun3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getKat3, token2);
				strcat(mkFolder, getKat3);
				//printf("3 : %s %s\n", getNama3, getKat3);
				strcat(kat3, getKat3);
				strcat(kat3, "/");
				strcat(kat3, getNama3);
				strcat(kat3, ".png");
			}
			
			makedir(mkFolder);
			
			//Copy gambar ke dir masing"
			copy(listFile, kat1);
			copy(listFile, kat2);
			copy(listFile, kat3);
			
			//Menghapus semua file bekas copy
			delete(listFile);

			// Construct new path from our base path
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			listFilesRecursively(path);
		}
	}
	closedir(dir);
}

// Membuat directory
void buat_dir() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child

    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    
    	child_id = fork();

  		if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  		}

  		if (child_id == 0) {
   		 // this is child

    		char *argv[] = {"unzip", "-j", "drakor", "*.png", "-d", "shift2/drakor", NULL};
    		execv("/bin/unzip", argv);
  		} else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		listFilesRecursively("shift2/drakor/");
    	}
  }
}

int main() {
	buat_dir();
return 0;
}

