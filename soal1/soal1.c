#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <syslog.h>
#include <json-c/json.h>


void downloadDatabase(char url[], char fileName[]){
    char *argv[7] = {"wget", "--no-check-certificate", url, "-O", fileName, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/wget", argv); 
    }
}

void extractZIP(char fileName[]){
    char *argv[4] = {"unzip", fileName, NULL};
    
    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/unzip", argv); 
    }
}

void createFolder(char path[]){
    char *argv[3] = {"mkdir", path, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/mkdir", argv); 
    }
}

int countFile(char directoryPath[]){
    int fileCount = 0;

    //Menghitung file
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);

    while(file != NULL){
        if(file->d_type == DT_REG){
            fileCount++;
        }
        file = readdir(path);
    }

    closedir(path);

    return fileCount;
}

struct json_object** loadFile(char directoryPath[], int fileCount){
    int i = 0;
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);
    
    char buffer[5120];
    struct json_object **parsed_json = malloc(sizeof(struct json_object*) * fileCount);
    char filePath[300];

     while(file != NULL){
        strcpy(filePath, directoryPath);

        if(file->d_type == DT_REG){
            
            strcat(filePath, file->d_name);
        
            FILE *fp;
            fp = fopen(filePath, "r");
            fread(buffer, 5120, 1, fp);
            fclose(fp);

            *(parsed_json + i) = json_tokener_parse(buffer);
            
            i++;
        }
        file = readdir(path);
    }

    closedir(path);

    return parsed_json;
}

int getRandomIndex(int low, int high){
    int index = (rand() %(high - low + 1)) + low;
    return index;
}

void moveFile(char parentPath[], char target[], char destinationPath[]){
    char *argv[11] = {"find", parentPath, "-name", target, "-exec", "mv", "-t", destinationPath, "{}", "+", NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/find", argv); 
    }
}

void moveFolder(char parentPath[], char target[], char destinationPath[]){
    char *argv[11] = {"find", parentPath, "-wholename", target, "-exec", "mv", "-t", destinationPath, "{}", "+", NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/find", argv); 
    }
}

void zip(char zipName[], char parentPath[], char password[]){
    char *argv[9] = {"zip", "--password", password, "-r", zipName, ".", "-i", parentPath, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/zip", argv); 
    }
}

void deleteFolder(char path[]){
    char *argv[4] = {"rm", "-r", path, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/rm", argv); 
    }
}

void start(){
    downloadDatabase("https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "Anggap_ini_database_characters.zip");
    downloadDatabase("https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "Anggap_ini_database_weapon.zip");
    
    sleep(4);

    extractZIP("Anggap_ini_database_weapon.zip");
    extractZIP("Anggap_ini_database_characters.zip");

    createFolder("/home/azzura13/gacha_gacha");

    sleep(2);

    //Mengimpor file JSON
    char charactersPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/characters/";
    char weaponsPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/weapons/";
    int characterCount;
    int weaponCount;

    struct json_object **parsed_characters;
    struct json_object **parsed_weapons;

    characterCount = countFile(charactersPath);
    parsed_characters = loadFile(charactersPath, characterCount);

    weaponCount = countFile(weaponsPath);
    parsed_weapons = loadFile(weaponsPath, weaponCount);

    sleep(2);

    //Memulai gacha
    int iteration = 1;
    int primogems = 79000;
    int index = 0;
    int txtIndex = 0;
    int folderIndex = 0;
    int prevFolderIndex = 0;
    int gachaSuccessCount = 0;

    char type[12];
    char resultList[11][350];
    char iterationChar[6];
    char primogemsChar[6];
    char txtIndexChar[4];
    char folderIndexChar[4];
    char filePath[200];
    char folderPath[200];

    struct json_object *name;
    struct json_object *rarity;

    char underscore[2] = "_";
    char newLine[3] = "\n";
    char colon[2] = ":";
    char underScore[2] = "_";
    char dottxt[5] = ".txt";
    char asterisk[2] = "*";
    char path[] = "/home/azzura13/gacha_gacha/";

    //Mengecek apakah sudah waktunya untuk memulai gacha (30 Maret pukul 04:44)
    srand(time(NULL));

    time_t current;
    struct tm *curr_time;
    int hour, minute, second;
    int day, month;

    current = time(NULL);
    curr_time = localtime(&current);

    day = curr_time->tm_mday;
    month = curr_time->tm_mon;
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;

    while(day != 30 || month != 2 || hour != 4 || minute != 44){
        current = time(NULL);
        curr_time = localtime(&current);

        day = curr_time->tm_mday;
        month = curr_time->tm_mon;
        hour = curr_time->tm_hour;
        minute = curr_time->tm_min;
    }

    //Membuat file .txt pertama untuk memulai

    //Mendapatkan waktu sistem

    current = time(NULL);
    curr_time = localtime(&current);

    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
    second = curr_time->tm_sec;

    //Mengubah format waktu menjadi string
    char hourStr[3], minStr[3], secStr[3];

    sprintf(hourStr, "%d", hour);
    sprintf(minStr, "%d", minute);
    sprintf(secStr, "%d", second);

    if(strlen(hourStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, hourStr);
        strcpy(hourStr, temp);
    }
    if(strlen(minStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, minStr);
        strcpy(minStr, temp);
    }
    if(strlen(secStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, secStr);
        strcpy(secStr, temp);
    }

    // Membuat nama file .txt
    // Menghitung sisa gacha yang bisa dilakukan

    if(primogems - 1600 >= 0)
        txtIndex += 10;
    else
        txtIndex += primogems / 160;

    sprintf(txtIndexChar, "%d", txtIndex);

    strcpy(filePath, path);
    strcat(filePath, hourStr);
    strcat(filePath, colon);
    strcat(filePath, minStr);
    strcat(filePath, colon);
    strcat(filePath, secStr);
    strcat(filePath, underScore);
    strcat(filePath, "gacha");
    strcat(filePath, underScore);
    strcat(filePath, txtIndexChar);
    strcat(filePath, dottxt);

    //Membuat file .txt pertama
    FILE *fp;

    fp = fopen(filePath, "w");
    fclose(fp);

    sleep(1);

    //Membuat folder pertama
    if(primogems - 14400 >= 0)
        folderIndex += 90;
    else
        folderIndex += primogems / 160;

    sprintf(folderIndexChar, "%d", folderIndex);
    strcpy(folderPath, path);
    strcat(folderPath, "total_gacha_");
    strcat(folderPath, folderIndexChar);

    createFolder(folderPath);
    
    //Memulai gacha
    while(1){
        if(primogems >= 160){
            primogems -= 160;

            if(iteration%2 == 0){
                //genap
                index = getRandomIndex(0, weaponCount - 1);
                json_object_object_get_ex(*(parsed_weapons + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_weapons + index), "name", &name);
                strcpy(type, "weapons");
            }else{
                //ganjil
                index = getRandomIndex(0, characterCount - 1);
                json_object_object_get_ex(*(parsed_characters + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_characters + index), "name", &name);
                strcpy(type, "characters");
            }

            gachaSuccessCount++;

        }else{
            break;
        }

        sprintf(iterationChar, "%d", iteration);
        sprintf(primogemsChar, "%d", primogems);

        strcpy(resultList[(iteration - 1) % 10], iterationChar);
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], type);
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], json_object_get_string(rarity));
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], json_object_get_string(name));
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], primogemsChar);
        strcat(resultList[(iteration - 1) % 10], newLine);

        //Memasukkan data ke .txt sebelumnya
        if(iteration%10 == 0){
            FILE *fp1;
            fp1 = fopen(filePath, "w");
            
            int i;
            for(i = 0; i < gachaSuccessCount; i++){
                fprintf(fp1, "%s", resultList[i]);
            }

            fclose(fp1);

            //Membuat file .txt

            //Mendapatkan waktu sistem
            current = time(NULL);
            curr_time = localtime(&current);

            hour = curr_time->tm_hour;
            minute = curr_time->tm_min;
            second = curr_time->tm_sec;

            //Mengubah format waktu menjadi string
            sprintf(hourStr, "%d", hour);
            sprintf(minStr, "%d", minute);
            sprintf(secStr, "%d", second);

            if(strlen(hourStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, hourStr);
                strcpy(hourStr, temp);
            }
            if(strlen(minStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, minStr);
                strcpy(minStr, temp);
            }
            if(strlen(secStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, secStr);
                strcpy(secStr, temp);
            }

            // Membuat nama file .txt

            // Menghitung sisa gacha yang bisa dilakukan
            if(primogems - 1600 >= 0)
                txtIndex += 10;
            else
                txtIndex += primogems / 160;

            sprintf(txtIndexChar, "%d", txtIndex);

            strcpy(filePath, path);
            strcat(filePath, hourStr);
            strcat(filePath, colon);
            strcat(filePath, minStr);
            strcat(filePath, colon);
            strcat(filePath, secStr);
            strcat(filePath, underScore);
            strcat(filePath, "gacha");
            strcat(filePath, underScore);
            strcat(filePath, txtIndexChar);
            strcat(filePath, dottxt);

            sleep(1);

            //Membuat file .txt
            FILE *fp2;

            fp2 = fopen(filePath, "w");
            fclose(fp2);

            gachaSuccessCount = 0;

        }else if(primogems < 160){
            FILE *fp1;
            fp1 = fopen(filePath, "w");
            
            int i;
            for(i = 0; i < gachaSuccessCount; i++){
                fprintf(fp1, "%s", resultList[i]);
            }

            fclose(fp1);

            gachaSuccessCount = 0;

        }

        
        // Membuat folder
        if(iteration%90 == 0){
            // Memindahkan file ke folder
            char fileName[150];
            char iterationStr[5];

            int i;
            
            for(i = iteration; i > iteration - 90; i -= 10){
                
                sprintf(iterationStr, "%d", i);

                strcpy(fileName, asterisk);
                strcat(fileName, iterationStr);
                strcat(fileName, dottxt);
                moveFile(path, fileName, folderPath);
                
            }
            
            prevFolderIndex = folderIndex;

            // Membuat folder selanjutnya
            if(primogems - 14400 >= 0)
                folderIndex += 90;
            else
                folderIndex += primogems / 160;

            
            sprintf(folderIndexChar, "%d", folderIndex);
            
            strcpy(folderPath, path);
            strcat(folderPath, "total_gacha_");
            strcat(folderPath, folderIndexChar);
            
            createFolder(folderPath);
            
        }else if(primogems < 160){
            // Memindahkan file ke folder
            char fileName[150];
            char iterationStr[5];
            
            int i;
            for(i = prevFolderIndex + 10; i < iteration; i += 10){
                sprintf(iterationStr, "%d", i);

                strcpy(fileName, asterisk);
                strcat(fileName, iterationStr);
                strcat(fileName, dottxt);
                moveFile(path, fileName, folderPath);
            }

            sprintf(iterationStr, "%d", iteration);

            strcpy(fileName, asterisk);
            strcat(fileName, iterationStr);
            strcat(fileName, dottxt);
            moveFile(path, fileName, folderPath);
        }
        
        iteration++;
    }

    //Mengecek apakah sudah waktunya untuk men-zip folder gacha (30 Maret pukul 07:44)
    current = time(NULL);
    curr_time = localtime(&current);

    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;

    while(hour != 7 || minute != 44){
        current = time(NULL);
        curr_time = localtime(&current);

        hour = curr_time->tm_hour;
        minute = curr_time->tm_min;
    }

    //Memindahkan semua folder gacha ke directory program
    int i;
    char tempName[100];
    int tempGachaCount;
    char tempGachaCountStr[4];

    for(i = 1; i <= folderIndex / 90; i++){
        tempGachaCount = i * 90;
        sprintf(tempGachaCountStr, "%d", tempGachaCount);
        strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
        strcat(tempName, tempGachaCountStr);
        moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");
    }

    
    sprintf(tempGachaCountStr, "%d", folderIndex);
    strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
    strcat(tempName, tempGachaCountStr);
    moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");

    sleep(2);
    
    //Men-zip folder gacha
    zip("not_safe_for_wibu.zip", "total_gacha_*", "satuduatiga");
    sleep(2);
    moveFile("/home/azzura13/PraktikumSisop/Modul2/soal1/", "not_safe_for_wibu.zip", "/home/azzura13/gacha_gacha");

    sleep(2);
    
    //Menghapus folder gacha
    for(i = 1; i <= folderIndex / 90; i++){
        tempGachaCount = i * 90;
        sprintf(tempGachaCountStr, "%d", tempGachaCount);
        strcpy(tempName, "total_gacha_");
        strcat(tempName, tempGachaCountStr);
        deleteFolder(tempName);
    }

    sprintf(tempGachaCountStr, "%d", folderIndex);
    strcpy(tempName, "total_gacha_");
    strcat(tempName, tempGachaCountStr);
    deleteFolder(tempName);
}

int main(){
    start();
}
