# Laporan Resmi Soal Shift Sistem Operasi Modul 2 F05 2022

Kelompok F05 :

                - Azzura Mahendra Putra Malinus     5025201211
                - Syaiful Bahri Dirgantara          5025201203
                - Wahyu Tri Saputro                 5025201217

## Soal 1
Soal meminta untuk membuat sebuah program simulasi sistem gacha. Akan diberikan dua buah database, characters dan weapons, yang akan digunakan untuk sistem gacha yang akan dibuat. Gacha akan dilakukan hingga alat tukar yang digunakan, yaitu primogems, habis digunakan. Ketika melakukan sekali gacha, hasil yang akan diperoleh adalah 10 hasil gacha (ganjil untuk character dan genap untuk weapon). Setelah itu, hasil gacha akan diletakkan pada sebuah file txt. Gacha akan dilakukan dengan interval satu detik. Setiap jumlah gacha mencapai kelipatan 90, file txt akan dimasukkan ke dalam sebuah folder.

Program simulasi sistem gacha ini akan dimulai pada tanggal 30 Maret pukul 04:44. Setelah tiga jam berlalu, program akan men-zip folder hasil gacha dan memberi file zip tersebut sebuah password. Setelah file zip dibuat, semua folder hasil gacha akan dihapus dan hanya menyisakan file zip tersebut saja.

### Pengerjaaan Soal

#### 1. Mengimpor beberapa header

```C
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <syslog.h>
#include <json-c/json.h>
```

#### 2. Membuat beberapa fungsi
#### Mengunduh database
```C
void downloadDatabase(char url[], char fileName[]){
    char *argv[7] = {"wget", "--no-check-certificate", url, "-O", fileName, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/wget", argv); 
    }
}
```
#### Mengekstrak zip
```C
void extractZIP(char fileName[]){
    char *argv[4] = {"unzip", fileName, NULL};
    
    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/unzip", argv); 
    }
}
```
#### Membuat folder
```C
void createFolder(char path[]){
    char *argv[3] = {"mkdir", path, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/mkdir", argv); 
    }
}
```
#### Menghitung file pada sebuah directory
```C
int countFile(char directoryPath[]){
    int fileCount = 0;

    //Menghitung file
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);

    while(file != NULL){
        if(file->d_type == DT_REG){
            fileCount++;
        }
        file = readdir(path);
    }

    closedir(path);

    return fileCount;
}
```
#### Membuka file JSON
```C
struct json_object** loadFile(char directoryPath[], int fileCount){
    int i = 0;
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);
    
    char buffer[5120];
    struct json_object **parsed_json = malloc(sizeof(struct json_object*) * fileCount);
    char filePath[300];

     while(file != NULL){
        strcpy(filePath, directoryPath);

        if(file->d_type == DT_REG){
            
            strcat(filePath, file->d_name);
        
            FILE *fp;
            fp = fopen(filePath, "r");
            fread(buffer, 5120, 1, fp);
            fclose(fp);

            *(parsed_json + i) = json_tokener_parse(buffer);
            
            i++;
        }
        file = readdir(path);
    }

    closedir(path);

    return parsed_json;
}
```
#### Mendapatkan nilai acak
```C
int getRandomIndex(int low, int high){
    int index = (rand() %(high - low + 1)) + low;
    return index;
}
```
#### Memindahkan file
```C
void moveFile(char parentPath[], char target[], char destinationPath[]){
    char *argv[11] = {"find", parentPath, "-name", target, "-exec", "mv", "-t", destinationPath, "{}", "+", NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/find", argv); 
    }
}
```
#### Memindahkan folder
```C
void moveFolder(char parentPath[], char target[], char destinationPath[]){
    char *argv[11] = {"find", parentPath, "-wholename", target, "-exec", "mv", "-t", destinationPath, "{}", "+", NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/find", argv); 
    }
}
```
#### Men-zip folder dengan password
```C
void zip(char zipName[], char parentPath[], char password[]){
    char *argv[9] = {"zip", "--password", password, "-r", zipName, ".", "-i", parentPath, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/zip", argv); 
    }
}
```
#### Menghapus folder
```C
void deleteFolder(char path[]){
    char *argv[4] = {"rm", "-r", path, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        //child
        execv("/usr/bin/rm", argv); 
    }
}
```
#### Fungsi start sebagai inti program
```C
void start(){
    downloadDatabase("https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "Anggap_ini_database_characters.zip");
    downloadDatabase("https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "Anggap_ini_database_weapon.zip");
    
    sleep(4);

    extractZIP("Anggap_ini_database_weapon.zip");
    extractZIP("Anggap_ini_database_characters.zip");

    createFolder("/home/azzura13/gacha_gacha");

    sleep(2);

    //Mengimpor file JSON
    char charactersPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/characters/";
    char weaponsPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/weapons/";
    int characterCount;
    int weaponCount;

    struct json_object **parsed_characters;
    struct json_object **parsed_weapons;

    characterCount = countFile(charactersPath);
    parsed_characters = loadFile(charactersPath, characterCount);

    weaponCount = countFile(weaponsPath);
    parsed_weapons = loadFile(weaponsPath, weaponCount);

    sleep(2);

    //Memulai gacha
    int iteration = 1;
    int primogems = 79000;
    int index = 0;
    int txtIndex = 0;
    int folderIndex = 0;
    int prevFolderIndex = 0;
    int gachaSuccessCount = 0;

    char type[12];
    char resultList[11][350];
    char iterationChar[6];
    char primogemsChar[6];
    char txtIndexChar[4];
    char folderIndexChar[4];
    char filePath[200];
    char folderPath[200];

    struct json_object *name;
    struct json_object *rarity;

    char underscore[2] = "_";
    char newLine[3] = "\n";
    char colon[2] = ":";
    char underScore[2] = "_";
    char dottxt[5] = ".txt";
    char asterisk[2] = "*";
    char path[] = "/home/azzura13/gacha_gacha/";

    //Mengecek apakah sudah waktunya untuk memulai gacha (30 Maret pukul 04:44)
    srand(time(NULL));

    time_t current;
    struct tm *curr_time;
    int hour, minute, second;
    int day, month;

    current = time(NULL);
    curr_time = localtime(&current);

    day = curr_time->tm_mday;
    month = curr_time->tm_mon;
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;

    while(day != 30 || month != 2 || hour != 4 || minute != 44){
        current = time(NULL);
        curr_time = localtime(&current);

        day = curr_time->tm_mday;
        month = curr_time->tm_mon;
        hour = curr_time->tm_hour;
        minute = curr_time->tm_min;
    }

    //Membuat file .txt pertama untuk memulai

    //Mendapatkan waktu sistem

    current = time(NULL);
    curr_time = localtime(&current);

    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
    second = curr_time->tm_sec;

    //Mengubah format waktu menjadi string
    char hourStr[3], minStr[3], secStr[3];

    sprintf(hourStr, "%d", hour);
    sprintf(minStr, "%d", minute);
    sprintf(secStr, "%d", second);

    if(strlen(hourStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, hourStr);
        strcpy(hourStr, temp);
    }
    if(strlen(minStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, minStr);
        strcpy(minStr, temp);
    }
    if(strlen(secStr) == 1){
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, secStr);
        strcpy(secStr, temp);
    }

    // Membuat nama file .txt
    // Menghitung sisa gacha yang bisa dilakukan

    if(primogems - 1600 >= 0)
        txtIndex += 10;
    else
        txtIndex += primogems / 160;

    sprintf(txtIndexChar, "%d", txtIndex);

    strcpy(filePath, path);
    strcat(filePath, hourStr);
    strcat(filePath, colon);
    strcat(filePath, minStr);
    strcat(filePath, colon);
    strcat(filePath, secStr);
    strcat(filePath, underScore);
    strcat(filePath, "gacha");
    strcat(filePath, underScore);
    strcat(filePath, txtIndexChar);
    strcat(filePath, dottxt);

    //Membuat file .txt pertama
    FILE *fp;

    fp = fopen(filePath, "w");
    fclose(fp);

    sleep(1);

    //Membuat folder pertama
    if(primogems - 14400 >= 0)
        folderIndex += 90;
    else
        folderIndex += primogems / 160;

    sprintf(folderIndexChar, "%d", folderIndex);
    strcpy(folderPath, path);
    strcat(folderPath, "total_gacha_");
    strcat(folderPath, folderIndexChar);

    createFolder(folderPath);
    
    //Memulai gacha
    while(1){
        if(primogems >= 160){
            primogems -= 160;

            if(iteration%2 == 0){
                //genap
                index = getRandomIndex(0, weaponCount - 1);
                json_object_object_get_ex(*(parsed_weapons + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_weapons + index), "name", &name);
                strcpy(type, "weapons");
            }else{
                //ganjil
                index = getRandomIndex(0, characterCount - 1);
                json_object_object_get_ex(*(parsed_characters + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_characters + index), "name", &name);
                strcpy(type, "characters");
            }

            gachaSuccessCount++;

        }else{
            break;
        }

        sprintf(iterationChar, "%d", iteration);
        sprintf(primogemsChar, "%d", primogems);

        strcpy(resultList[(iteration - 1) % 10], iterationChar);
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], type);
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], json_object_get_string(rarity));
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], json_object_get_string(name));
        strcat(resultList[(iteration - 1) % 10], underscore);
        strcat(resultList[(iteration - 1) % 10], primogemsChar);
        strcat(resultList[(iteration - 1) % 10], newLine);

        //Memasukkan data ke .txt sebelumnya
        if(iteration%10 == 0){
            FILE *fp1;
            fp1 = fopen(filePath, "w");
            
            int i;
            for(i = 0; i < gachaSuccessCount; i++){
                fprintf(fp1, "%s", resultList[i]);
            }

            fclose(fp1);

            //Membuat file .txt

            //Mendapatkan waktu sistem
            current = time(NULL);
            curr_time = localtime(&current);

            hour = curr_time->tm_hour;
            minute = curr_time->tm_min;
            second = curr_time->tm_sec;

            //Mengubah format waktu menjadi string
            sprintf(hourStr, "%d", hour);
            sprintf(minStr, "%d", minute);
            sprintf(secStr, "%d", second);

            if(strlen(hourStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, hourStr);
                strcpy(hourStr, temp);
            }
            if(strlen(minStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, minStr);
                strcpy(minStr, temp);
            }
            if(strlen(secStr) == 1){
                char temp[3];
                strcpy(temp, "0");
                strcat(temp, secStr);
                strcpy(secStr, temp);
            }

            // Membuat nama file .txt

            // Menghitung sisa gacha yang bisa dilakukan
            if(primogems - 1600 >= 0)
                txtIndex += 10;
            else
                txtIndex += primogems / 160;

            sprintf(txtIndexChar, "%d", txtIndex);

            strcpy(filePath, path);
            strcat(filePath, hourStr);
            strcat(filePath, colon);
            strcat(filePath, minStr);
            strcat(filePath, colon);
            strcat(filePath, secStr);
            strcat(filePath, underScore);
            strcat(filePath, "gacha");
            strcat(filePath, underScore);
            strcat(filePath, txtIndexChar);
            strcat(filePath, dottxt);

            sleep(1);

            //Membuat file .txt
            FILE *fp2;

            fp2 = fopen(filePath, "w");
            fclose(fp2);

            gachaSuccessCount = 0;

        }else if(primogems < 160){
            FILE *fp1;
            fp1 = fopen(filePath, "w");
            
            int i;
            for(i = 0; i < gachaSuccessCount; i++){
                fprintf(fp1, "%s", resultList[i]);
            }

            fclose(fp1);

            gachaSuccessCount = 0;

        }

        
        // Membuat folder
        if(iteration%90 == 0){
            // Memindahkan file ke folder
            char fileName[150];
            char iterationStr[5];

            int i;
            
            for(i = iteration; i > iteration - 90; i -= 10){
                
                sprintf(iterationStr, "%d", i);

                strcpy(fileName, asterisk);
                strcat(fileName, iterationStr);
                strcat(fileName, dottxt);
                moveFile(path, fileName, folderPath);
                
            }
            
            prevFolderIndex = folderIndex;

            // Membuat folder selanjutnya
            if(primogems - 14400 >= 0)
                folderIndex += 90;
            else
                folderIndex += primogems / 160;

            
            sprintf(folderIndexChar, "%d", folderIndex);
            
            strcpy(folderPath, path);
            strcat(folderPath, "total_gacha_");
            strcat(folderPath, folderIndexChar);
            
            createFolder(folderPath);
            
        }else if(primogems < 160){
            // Memindahkan file ke folder
            char fileName[150];
            char iterationStr[5];
            
            int i;
            for(i = prevFolderIndex + 10; i < iteration; i += 10){
                sprintf(iterationStr, "%d", i);

                strcpy(fileName, asterisk);
                strcat(fileName, iterationStr);
                strcat(fileName, dottxt);
                moveFile(path, fileName, folderPath);
            }

            sprintf(iterationStr, "%d", iteration);

            strcpy(fileName, asterisk);
            strcat(fileName, iterationStr);
            strcat(fileName, dottxt);
            moveFile(path, fileName, folderPath);
        }
        
        iteration++;
    }

    //Mengecek apakah sudah waktunya untuk men-zip folder gacha (30 Maret pukul 07:44)
    current = time(NULL);
    curr_time = localtime(&current);

    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;

    while(hour != 7 || minute != 44){
        current = time(NULL);
        curr_time = localtime(&current);

        hour = curr_time->tm_hour;
        minute = curr_time->tm_min;
    }

    //Memindahkan semua folder gacha ke directory program
    int i;
    char tempName[100];
    int tempGachaCount;
    char tempGachaCountStr[4];

    for(i = 1; i <= folderIndex / 90; i++){
        tempGachaCount = i * 90;
        sprintf(tempGachaCountStr, "%d", tempGachaCount);
        strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
        strcat(tempName, tempGachaCountStr);
        moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");
    }

    
    sprintf(tempGachaCountStr, "%d", folderIndex);
    strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
    strcat(tempName, tempGachaCountStr);
    moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");

    sleep(2);
    
    //Men-zip folder gacha
    zip("not_safe_for_wibu.zip", "total_gacha_*", "satuduatiga");
    sleep(2);
    moveFile("/home/azzura13/PraktikumSisop/Modul2/soal1/", "not_safe_for_wibu.zip", "/home/azzura13/gacha_gacha");

    sleep(2);
    
    //Menghapus folder gacha
    for(i = 1; i <= folderIndex / 90; i++){
        tempGachaCount = i * 90;
        sprintf(tempGachaCountStr, "%d", tempGachaCount);
        strcpy(tempName, "total_gacha_");
        strcat(tempName, tempGachaCountStr);
        deleteFolder(tempName);
    }

    sprintf(tempGachaCountStr, "%d", folderIndex);
    strcpy(tempName, "total_gacha_");
    strcat(tempName, tempGachaCountStr);
    deleteFolder(tempName);
}
```
#### 3. Penjelasan fungsi start
Fungsi start dijalankan pada fungsi main
```C
int main(){
    start();
}
```
Mengunduh kedua database
```C
downloadDatabase("https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "Anggap_ini_database_characters.zip");
downloadDatabase("https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "Anggap_ini_database_weapon.zip");
    
sleep(4);
```
Mengekstrak file zip dari database (diletakkan di folder yang sama dengan program)
```C
extractZIP("Anggap_ini_database_weapon.zip");
extractZIP("Anggap_ini_database_characters.zip");
```
Membuat folder `gacha_gacha`
```C
createFolder("/home/azzura13/gacha_gacha");

sleep(2);
```
Mengimpor file JSON dari database. Seluruh file JSON dari folder characters akan dimasukkan ke dalam array of JSON object parsed_characters dan seluruh file JSON dari folder weapon akan dimasukkan ke dalam array of JSON object parsed_weapons.
```C
//Mengimpor file JSON
char charactersPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/characters/";
char weaponsPath[] = "/home/azzura13/PraktikumSisop/Modul2/soal1/weapons/";
int characterCount;
int weaponCount;

struct json_object **parsed_characters;
struct json_object **parsed_weapons;

characterCount = countFile(charactersPath);
parsed_characters = loadFile(charactersPath, characterCount);

weaponCount = countFile(weaponsPath);
parsed_weapons = loadFile(weaponsPath, weaponCount);

sleep(2);
```
Membuat beberapa variabel dan konstan
```C
int iteration = 1;
int primogems = 79000;
int index = 0;
int txtIndex = 0;
int folderIndex = 0;
int prevFolderIndex = 0;
int gachaSuccessCount = 0;

char type[12];
char resultList[11][350];
char iterationChar[6];
char primogemsChar[6];
char txtIndexChar[4];
char folderIndexChar[4];
char filePath[200];
char folderPath[200];

struct json_object *name;
struct json_object *rarity;

char underscore[2] = "_";
char newLine[3] = "\n";
char colon[2] = ":";
char underScore[2] = "_";
char dottxt[5] = ".txt";
char asterisk[2] = "*";
char path[] = "/home/azzura13/gacha_gacha/";
```
Membuat kondisi untuk melanjutkan program pada tanggal 30 Maret pukul 04.44
```C
//Mengecek apakah sudah waktunya untuk memulai gacha (30 Maret pukul 04:44)
srand(time(NULL));

time_t current;
struct tm *curr_time;
int hour, minute, second;
int day, month;

current = time(NULL);
curr_time = localtime(&current);

day = curr_time->tm_mday;
month = curr_time->tm_mon;
hour = curr_time->tm_hour;
minute = curr_time->tm_min;

while(day != 30 || month != 2 || hour != 4 || minute != 44){
    current = time(NULL);
    curr_time = localtime(&current);

    day = curr_time->tm_mday;
    month = curr_time->tm_mon;
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
}
```
Membuat file txt pertama untuk menyimpan hasil gacha selanjutnya
```C
//Mendapatkan waktu sistem

current = time(NULL);
curr_time = localtime(&current);

hour = curr_time->tm_hour;
minute = curr_time->tm_min;
second = curr_time->tm_sec;

//Mengubah format waktu menjadi string
char hourStr[3], minStr[3], secStr[3];

sprintf(hourStr, "%d", hour);
sprintf(minStr, "%d", minute);
sprintf(secStr, "%d", second);

if(strlen(hourStr) == 1){
    char temp[3];
    strcpy(temp, "0");
    strcat(temp, hourStr);
    strcpy(hourStr, temp);
}
if(strlen(minStr) == 1){
    char temp[3];
    strcpy(temp, "0");
    strcat(temp, minStr);
    strcpy(minStr, temp);
}
if(strlen(secStr) == 1){
    char temp[3];
    strcpy(temp, "0");
    strcat(temp, secStr);
    strcpy(secStr, temp);
}

// Membuat nama file .txt
// Menghitung sisa gacha yang bisa dilakukan

if(primogems - 1600 >= 0)
    txtIndex += 10;
else
    txtIndex += primogems / 160;

sprintf(txtIndexChar, "%d", txtIndex);

strcpy(filePath, path);
strcat(filePath, hourStr);
strcat(filePath, colon);
strcat(filePath, minStr);
strcat(filePath, colon);
strcat(filePath, secStr);
strcat(filePath, underScore);
strcat(filePath, "gacha");
strcat(filePath, underScore);
strcat(filePath, txtIndexChar);
strcat(filePath, dottxt);

//Membuat file .txt pertama
FILE *fp;

fp = fopen(filePath, "w");
fclose(fp);

sleep(1);
```
Membuat folder pertama untuk menyimpan hasil gacha selanjutnya
```C
if(primogems - 14400 >= 0)
    folderIndex += 90;
else
    folderIndex += primogems / 160;

sprintf(folderIndexChar, "%d", folderIndex);
strcpy(folderPath, path);
strcat(folderPath, "total_gacha_");
strcat(folderPath, folderIndexChar);

createFolder(folderPath);
```
Memulai gacha, melakukan gacha hingga primogem habis
```C
while(1){
    // Mengecek apakah masih bisa untuk melakukan gacha, jika tidak, keluar dari perulangan
    if(primogems >= 160){
        primogems -= 160;

        if(iteration%2 == 0){
            // genap, melakukan gacha weapon
            index = getRandomIndex(0, weaponCount - 1);
            json_object_object_get_ex(*(parsed_weapons + index), "rarity", &rarity);
            json_object_object_get_ex(*(parsed_weapons + index), "name", &name);
            strcpy(type, "weapons");
        }else{
            // ganjil, melakukan gacha character
            index = getRandomIndex(0, characterCount - 1);
            json_object_object_get_ex(*(parsed_characters + index), "rarity", &rarity);
            json_object_object_get_ex(*(parsed_characters + index), "name", &name);
            strcpy(type, "characters");
        }

        gachaSuccessCount++;

    }else{
        break;
    }

    // Menyimpan hasil gacha hingga sepuluh baris
    sprintf(iterationChar, "%d", iteration);
    sprintf(primogemsChar, "%d", primogems);

    strcpy(resultList[(iteration - 1) % 10], iterationChar);
    strcat(resultList[(iteration - 1) % 10], underscore);
    strcat(resultList[(iteration - 1) % 10], type);
    strcat(resultList[(iteration - 1) % 10], underscore);
    strcat(resultList[(iteration - 1) % 10], json_object_get_string(rarity));
    strcat(resultList[(iteration - 1) % 10], underscore);
    strcat(resultList[(iteration - 1) % 10], json_object_get_string(name));
    strcat(resultList[(iteration - 1) % 10], underscore);
    strcat(resultList[(iteration - 1) % 10], primogemsChar);
    strcat(resultList[(iteration - 1) % 10], newLine);

    //Memasukkan data ke .txt sebelumnya jika jumlah gacha kelipatan sepuluh
    if(iteration%10 == 0){
        FILE *fp1;
        fp1 = fopen(filePath, "w");
        
        int i;
        for(i = 0; i < gachaSuccessCount; i++){
            fprintf(fp1, "%s", resultList[i]);
        }

        fclose(fp1);

        //Membuat file .txt untuk gacha selanjutnya

        //Mendapatkan waktu sistem
        current = time(NULL);
        curr_time = localtime(&current);

        hour = curr_time->tm_hour;
        minute = curr_time->tm_min;
        second = curr_time->tm_sec;

        //Mengubah format waktu menjadi string
        sprintf(hourStr, "%d", hour);
        sprintf(minStr, "%d", minute);
        sprintf(secStr, "%d", second);

        if(strlen(hourStr) == 1){
            char temp[3];
            strcpy(temp, "0");
            strcat(temp, hourStr);
            strcpy(hourStr, temp);
        }
        if(strlen(minStr) == 1){
            char temp[3];
            strcpy(temp, "0");
            strcat(temp, minStr);
            strcpy(minStr, temp);
        }
        if(strlen(secStr) == 1){
            char temp[3];
            strcpy(temp, "0");
            strcat(temp, secStr);
            strcpy(secStr, temp);
        }

        // Membuat nama file .txt

        // Menghitung sisa gacha yang bisa dilakukan
        if(primogems - 1600 >= 0)
            txtIndex += 10;
        else
            txtIndex += primogems / 160;

        sprintf(txtIndexChar, "%d", txtIndex);

        strcpy(filePath, path);
        strcat(filePath, hourStr);
        strcat(filePath, colon);
        strcat(filePath, minStr);
        strcat(filePath, colon);
        strcat(filePath, secStr);
        strcat(filePath, underScore);
        strcat(filePath, "gacha");
        strcat(filePath, underScore);
        strcat(filePath, txtIndexChar);
        strcat(filePath, dottxt);

        sleep(1);

        //Membuat file .txt
        FILE *fp2;

        fp2 = fopen(filePath, "w");
        fclose(fp2);

        gachaSuccessCount = 0;

    // Jika primogems kurang dari 160, langsung memasukkan hasil gacha yang berhasil dilakukan
    }else if(primogems < 160){
        FILE *fp1;
        fp1 = fopen(filePath, "w");
        
        int i;
        for(i = 0; i < gachaSuccessCount; i++){
            fprintf(fp1, "%s", resultList[i]);
        }

        fclose(fp1);

        gachaSuccessCount = 0;

    }

    
    // Memindahkan txt yang ada ke folder yang dibuat sebelumnya jika jumlah gacha kelipatan 90
    if(iteration%90 == 0){
        char fileName[150];
        char iterationStr[5];

        int i;
        
        for(i = iteration; i > iteration - 90; i -= 10){
            
            sprintf(iterationStr, "%d", i);

            strcpy(fileName, asterisk);
            strcat(fileName, iterationStr);
            strcat(fileName, dottxt);
            moveFile(path, fileName, folderPath);
            
        }
        
        prevFolderIndex = folderIndex;

        // Membuat folder untuk hasil gacha selanjutnya
        if(primogems - 14400 >= 0)
            folderIndex += 90;
        else
            folderIndex += primogems / 160;

        
        sprintf(folderIndexChar, "%d", folderIndex);
        
        strcpy(folderPath, path);
        strcat(folderPath, "total_gacha_");
        strcat(folderPath, folderIndexChar);
        
        createFolder(folderPath);
    // Jika primogems kurang dari 160, langsung memindahkan txt yang tersisa ke folder yang dibuat sebelumnya
    }else if(primogems < 160){
        
        char fileName[150];
        char iterationStr[5];
        
        int i;
        for(i = prevFolderIndex + 10; i < iteration; i += 10){
            sprintf(iterationStr, "%d", i);

            strcpy(fileName, asterisk);
            strcat(fileName, iterationStr);
            strcat(fileName, dottxt);
            moveFile(path, fileName, folderPath);
        }

        sprintf(iterationStr, "%d", iteration);

        strcpy(fileName, asterisk);
        strcat(fileName, iterationStr);
        strcat(fileName, dottxt);
        moveFile(path, fileName, folderPath);
    }
    
    iteration++;
    }
```
Membuat kondisi untuk melanjutkan program tiga jam setelah memulai gacha (pukul 07:44)
```C
current = time(NULL);
curr_time = localtime(&current);

hour = curr_time->tm_hour;
minute = curr_time->tm_min;

while(hour != 7 || minute != 44){
    current = time(NULL);
    curr_time = localtime(&current);

    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
}
```
Memindahkan semua folder gacha ke directory program lalu men-zip semuanya (agar directory di dalam file zip sesuai dengan yang diinginkan). Setelah itu, memindahkan file zip kembali ke folder `gacha_gacha` dan menghapus semua folder gacha di directory program.
```C
//Memindahkan semua folder gacha ke directory program
int i;
char tempName[100];
int tempGachaCount;
char tempGachaCountStr[4];

for(i = 1; i <= folderIndex / 90; i++){
    tempGachaCount = i * 90;
    sprintf(tempGachaCountStr, "%d", tempGachaCount);
    strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
    strcat(tempName, tempGachaCountStr);
    moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");
}


sprintf(tempGachaCountStr, "%d", folderIndex);
strcpy(tempName, "/home/azzura13/gacha_gacha/total_gacha_");
strcat(tempName, tempGachaCountStr);
moveFolder(path, tempName, "/home/azzura13/PraktikumSisop/Modul2/soal1");

sleep(2);

//Men-zip folder gacha
zip("not_safe_for_wibu.zip", "total_gacha_*", "satuduatiga");
sleep(2);
moveFile("/home/azzura13/PraktikumSisop/Modul2/soal1/", "not_safe_for_wibu.zip", "/home/azzura13/gacha_gacha");

sleep(2);

//Menghapus folder gacha
for(i = 1; i <= folderIndex / 90; i++){
    tempGachaCount = i * 90;
    sprintf(tempGachaCountStr, "%d", tempGachaCount);
    strcpy(tempName, "total_gacha_");
    strcat(tempName, tempGachaCountStr);
    deleteFolder(tempName);
}

sprintf(tempGachaCountStr, "%d", folderIndex);
strcpy(tempName, "total_gacha_");
strcat(tempName, tempGachaCountStr);
deleteFolder(tempName);
```
Hasil akhir dari program adalah folder gacha_gacha yang berisi file zip `not_safe_for_wibu.zip` yang diberi password `satuduatiga`.

### Kendala dalam Pengerjaan
Saat pengerjaan soal 1, terdapat kendala pada pengimporan file database yang berupa JSON ke program C. Kendala tersebut telah diatasi dengan membuat array of JSON object (dibuat dengan malloc) yang akan menyimpan setiap objek JSON dari database, yaitu setiap file JSON yang ada di dalam folder database. Setelah semua file dimasukkan ke array of JSON object, setiap file bisa diakses seperti mengakses array pada umumnya. Oleh karena itu, melakukan dynamic memory allocation menggunakan malloc adalah cara yang cukup baik untuk mengimpor beberapa file JSON ke program C.

### Dokumentasi
#### 1. Pembuatan file txt dan pemindahannya ke folder (dimulai tanggal 30 Maret pukul 04.44)
![dokum_1.1](Dokumentasi/Soal1/Dokum_modul2_isi_folder_gacha.png)
![dokum_1.2](Dokumentasi/Soal1/Dokum_modul2_isi_folder.png)
#### 2. Contoh isi file txt (hasil gacha)
![dokum_1.3](Dokumentasi/Soal1/Dokum_modul2_contoh_isi_txt.png)
#### 3. Pembuatan file zip `not_safe_for_wibu.zip` (dimulai pukul 07.44)
![dokum_1.4](Dokumentasi/Soal1/Dokum_modul2_file_zip.png)
#### 4. File zip yang dilindungi password
![dokum_1.5](Dokumentasi/Soal1/Dokum_modul2_file_zip_dengan_password.png)

## Soal 2
Soal meminta untuk mengextract file zip yang sudah ada yaitu `drakor.zip` ke dalam folder "/home/[USER]/shift2/drakor". Kemudian soal meminta program untuk menghapus file-file dan folder-folder yang tidak digunakan, dimana file yang digunakan hanyalah yang formatnya `.png`. Selanjutnya program diminta untuk membuat folder-folder sesuai kategori setiap poster film yang ada pada gambar, dilanjutkan dengan memasukkan poster-poster film tersebut sesuai kategorinya. Terdapat gambar yang berisi 2 poster film yang harus dipisah dan dimasukkan sendiri-sendiri sesuai kategorinya. Setelah itu program diminta untuk membuat sebuah file `data.txt` yang berisi nama dan tahun rilis poster film drakor tersebut.

### Pengerjaan Soal

#### 1. Mengimpor beberapa header

```C
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
```

#### 2. Membuat fungsi untuk membuat folder/directory

```C
void makedir(char *dest) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		printf("%s\n", dest);
		char *argv[] = {"mkdir", "-p", dest, NULL};
		execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```

#### 3. Membuat fungsi untuk menghapus file

```C
void delete (char *file) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"rm", "-d", file, NULL};
		execv("/usr/bin/rm", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```

#### 4. Membuat fungsi untuk mengcopy file

```C
void copy(char *src, char *dest) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"cp", "-n", src, dest, NULL};
		execv("/usr/bin/cp", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```

#### 5. Membuat fungsi untuk menghapus 4 karakter di belakang yaitu ".png"

```C
char* cut_four (char* s) {
    int n;
    int i;
    char* new;
    for (i = 0; s[i] != '\0'; i++);
    // lenght of the new string
    n = i - 4 + 1;
    if (n < 1)
        return NULL;
    new = (char*) malloc (n * sizeof(char));
    for (i = 0; i < n - 1; i++)
        new[i] = s[i];
    new[i] = '\0';
    return new;
}
```

#### 6. Membuat fungsi untuk membuat folder kategori dan memasukkan tiap poster drakor

```C
void listFilesRecursively(char *basePath) {
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
		return;

	while ((dp = readdir(dir)) != NULL) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			char tempFolder[100];
			char mkFolder[100] = "shift2/drakor/";
			char kat1[100] = "shift2/drakor/";
			char kat2[1000] = "shift2/drakor/";
			char kat3[1000] = "shift/drakor/";
			char temp[1000], temp2[1000], temp3[1000], temp4[1000], getNama[1000], getTahun[1000], getKat[100], getNama2[1000], getTahun2[1000], getKat2[100], getNama3[1000], getTahun3[1000], getKat3[100];
			char *token, *token2, *token3;

			//strcpy(tempFolder, dp->d_name);
			//printf("%s\n", dp->d_name);
			//strtok(tempFolder, ";");
			//strcat(mkFolder, tempFolder);

			char listFile[100] = "shift2/drakor/";
			strcat(listFile, dp->d_name);

			if (!(strstr(dp->d_name, "_"))) {
				strcpy(temp, dp->d_name); 
				token = strtok(temp, ";"); 
				strcpy(getNama, token);
				token = strtok(NULL, ";"); 
				strcpy(getTahun, token);
				token = strtok(NULL, ";"); 
				strcpy(getKat, token);
				strcat(mkFolder, getKat);
				//printf("1 : %s %s\n", getNama, getKat);
				strcat(kat1, getKat);
				strcat(kat1, "/");
				strcat(kat1, getNama);
				strcat(kat1, ".png");
			}
			
			if (strstr(dp->d_name, "_")) {
				strcpy(temp3, dp->d_name); 
				token3 = strtok(temp3, ";"); 
				strcpy(getNama2, token3);
				token3 = strtok(NULL, ";");
				strcpy(getTahun2, token3);
				token3 = strtok(NULL, ";"); 
				strcpy(getKat2, token3);
				//strcat(mkFolder, getKat2);
				//printf("2 : %s %s\n", getNama2, getKat2);
				strcat(kat2, getKat2);
				strcat(kat2, "/");
				strcat(kat2, getNama2);
				strcat(kat2, ".png");
				
				strcpy(temp2, dp->d_name); 
				token2 = strtok(temp2, "_"); 
				token2 = strtok(NULL, "_");
				strcpy(temp4, token2);
				token2 = strtok(temp4, ";");
				strcpy(getNama3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getTahun3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getKat3, token2);
				strcat(mkFolder, getKat3);
				//printf("3 : %s %s\n", getNama3, getKat3);
				strcat(kat3, getKat3);
				strcat(kat3, "/");
				strcat(kat3, getNama3);
				strcat(kat3, ".png");
			}
			
			makedir(mkFolder);
			
			//Copy gambar ke dir masing"
			copy(listFile, kat1);
			copy(listFile, kat2);
			copy(listFile, kat3);
			
			//Menghapus semua file bekas copy
			delete(listFile);

			// Construct new path from our base path
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			listFilesRecursively(path);
		}
	}
	closedir(dir);
}
```

##### Penjelasan
Merupakan fungsi yang rekursif pada setiap file.

```C
if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
```
Kondisi untuk setiap file yang bukan `.` atau `..`.

```C
char listFile[100] = "shift2/drakor/";
			strcat(listFile, dp->d_name)
```
Memasukkan tiap nama file ke `listFile`

```C
if (!(strstr(dp->d_name, "_"))) {
				strcpy(temp, dp->d_name); 
				token = strtok(temp, ";"); 
				strcpy(getNama, token);
				token = strtok(NULL, ";"); 
				strcpy(getTahun, token);
				token = strtok(NULL, ";"); 
				strcpy(getKat, token);
				strcat(mkFolder, getKat);
				//printf("1 : %s %s\n", getNama, getKat);
				strcat(kat1, getKat);
				strcat(kat1, "/");
				strcat(kat1, getNama);
				strcat(kat1, ".png");
			}
```
Mencari di setiap nama file. Mengcopy tiap string sebelum `;` dan memasukkannya sesuai kedudukannya, yaitu nama, tahun, dan kateogori. Kemudian memasukkan nama kategori pada `mkFolder`

```C
if (strstr(dp->d_name, "_")) {
				strcpy(temp3, dp->d_name); 
				token3 = strtok(temp3, ";"); 
				strcpy(getNama2, token3);
				token3 = strtok(NULL, ";");
				strcpy(getTahun2, token3);
				token3 = strtok(NULL, ";"); 
				strcpy(getKat2, token3);
				//strcat(mkFolder, getKat2);
				//printf("2 : %s %s\n", getNama2, getKat2);
				strcat(kat2, getKat2);
				strcat(kat2, "/");
				strcat(kat2, getNama2);
				strcat(kat2, ".png");
				
				strcpy(temp2, dp->d_name); 
				token2 = strtok(temp2, "_"); 
				token2 = strtok(NULL, "_");
				strcpy(temp4, token2);
				token2 = strtok(temp4, ";");
				strcpy(getNama3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getTahun3, token2);
				token2 = strtok(NULL, ";"); 
				strcpy(getKat3, token2);
				strcat(mkFolder, getKat3);
				//printf("3 : %s %s\n", getNama3, getKat3);
				strcat(kat3, getKat3);
				strcat(kat3, "/");
				strcat(kat3, getNama3);
				strcat(kat3, ".png");
			}
```
Perlakuan yang sama seperti penjelasan sebelumnya, namun untuk file yang berisi 2 poster sekaligus dengan mencari tanda `_` di setiap nama file untuk pembeda kedua poster

```C
makedir(mkFolder);
```
Membuat directory apa saja yang ada di dalam `mkFolder`

```C
copy(listFile, kat1);
			copy(listFile, kat2);
			copy(listFile, kat3);
```
Mengcopy semua gambar ke dalam folder kategori masing-masing

```C
delete(listFile);
```
Menghapus semua file bekas copy

```C
            strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			listFilesRecursively(path);
```
Membuat path baru dari path sekarang dan kondisi rekursif

#### 7. Membuat fungsi utama

```C
void buat_dir() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child

    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    
    	child_id = fork();

  		if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  		}

  		if (child_id == 0) {
   		 // this is child

    		char *argv[] = {"unzip", "-j", "drakor", "*.png", "-d", "shift2/drakor", NULL};
    		execv("/bin/unzip", argv);
  		} else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		listFilesRecursively("shift2/drakor/");
    	}
  }
}
```

##### Penjelasan

```C
  pid_t child_id;
  int status;

  child_id = fork();
```
Membuat proses anak

```C
  if (child_id == 0) {
    // this is child

    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
  }
```
Membuat directory `/home/[USER]/shift2/drakor`

```C
else {
    // this is parent
    while ((wait(&status)) > 0);
    
    	child_id = fork();

  		if (child_id < 0) {
    		exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  		}
```
Membuat proses anak lagi di dalam proses orang tukar

```C
if (child_id == 0) {
   		 // this is child

    		char *argv[] = {"unzip", "-j", "drakor", "*.png", "-d", "shift2/drakor", NULL};
    		execv("/bin/unzip", argv);
  		}
```
Mengunzip file `drakor.zip` yang formatnya `.png`

```C
else {
    		// this is parent
    		while ((wait(&status)) > 0);
    		listFilesRecursively("shift2/drakor/");
    	}
```
Menjalankan fungsi `listFilesRecursively`

#### 8. Membuat fungsi main yang menjalankan fungsi utama

```C
int main() {
	buat_dir();
return 0;
}
```

### Kendala dalam Pengerjaaan
Dalam pengerjaan soal ini terdapat kendala yaitu folder kategori kategori terbuat double dimana salah satunya masih ada yang berimbuhan `.png`. Selain itu, pemisahan 1 gambar menjadi 2 poster drakor juga masih tidak sempurna sehingga masih ada poster yang belum terdeteksi dan masuk ke dalam foldernya.

### Dokumentasi

![dokum_2.1](Dokumentasi/Soal2/dokum2_1.jpg)

## Soal 3
Soal meminta untuk membuat folder darat, lalu jeda 3 detik membuat folder laut di dalam “/home/[USER]/modul2/”. Kemudian soal meminta program untuk mengextract animals.zip ke dalam folder modul2. Setelah diextract, program diminta untuk memisahkan hewan darat dan laut kemudian dimasukkan ke dalam masing - masing folder sesuai kategori. Namun, file animals yang masuk kategori bird harus dihapus dari folder darat. Terakhir program diminta untuk membuat file list.txt di folder air dan membuat list semua nama hewan air ke dalam list.txt.

### Pengerjaan Soal

#### 1. Mengimpor beberapa header

```bash
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>
#include <stdlib.h>
```

#### 2. Membuat variabel global
Variabel tersebut berisi argumen - argumen untuk menjalankan perintah - perintah yang diminta oleh soal. Contohnya adalah untuk membuat folder.

```bash
char keyMkdir[] = "/usr/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

#### 3. Membuat beberapa fungsi
Fungsi ini nantinya digunakan untuk membuat folder, mengextract animals.zip dan membuat list.txt.

```bash
void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if(child_id == 0)
    execv(str, argv);
  else
    while(wait(&status) > 0);
}
```
Fungsi tersebut digunakan untuk membuat sebuah process.

```bash
void createFolderDarat()
{
  char *argv[] = {"mkdir", "/home/wahyu/modul2/darat", NULL};
  createProcess(keyMkdir, argv);
}

void createFolderAir()
{
  char *argv[] = {"mkdir", "/home/wahyu/modul2/air", NULL};
  createProcess(keyMkdir, argv);
}
```
Kemudian membuat folder darat dan air

```bash
void unzipAnimalZip()
{
  char *argv[] = {"unzip", "animal.zip", NULL};
  createProcess(keyUnzip, argv);
}
```
Selanjutnya untuk mengextract animals.zip

```bash
void moveFile(char *folder, char *source)
{
  char coreFolder[] = "/home/wahyu/modul2/";
  strcat(coreFolder, folder);
  char *argv[] = {"mv", source, coreFolder, NULL};
  createProcess(keyMove, argv);
}
```
Memindahkan file

```bash
void removeFile(char *source)
{
  char *argv[] = {"rm", source, NULL};
  createProcess(keyRemove, argv);
}
```
Menghapus file

```bash
int listAnimals()
{
    DIR *dp;
    struct dirent *ep;
    pid_t childs;
    int status = 0;

  childs = fork();

  if(childs == 0)
  {
    dp = opendir("animal");
    if(dp != NULL)
    {
      while((ep = readdir(dp)))
      {
        if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containDarat = strstr(ep->d_name, "darat");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if(containDarat)
          {
            moveFile("darat", source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
  else
  {
    while(wait(&status) > 0);
    sleep(3);
    dp = opendir("animal");
    if(dp != NULL)
    {
      while((ep = readdir(dp)))
      {
        if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
        {
          char *containAir = strstr(ep->d_name, "air");

          char source[100] = "animal/";
          strcat(source, ep->d_name);

          if(containAir)
          {
            moveFile("air", source);
          }
          else
          {
            removeFile(source);
          }
        }
      }
      (void)closedir(dp);
    }
  }
}
```
Fungsi tersebut untuk mengurutkan isi dari animals

```bash
void removeBirdFromDarat()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("darat");

  if(dp != NULL)
  {
    while((ep = readdir(dp)))
    {
      if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char *containBird = strstr(ep->d_name, "bird");

        char source[100] = "darat/";
        strcat(source, ep->d_name);

        if(containBird)
        {
          removeFile(source);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Fungsi tersebut digunakan untuk menghapus bird dari kategori darat.

```bash
void createTxt(char *where)
{
  char *argv[] = {"touch", where, NULL};
  createProcess(keyTouch, argv);
}

void writeTxt(char *folder, char *file)
{
  // dir
  DIR *dp;
  struct dirent *ep;
  dp = opendir(folder);

  // file
  FILE *f = fopen(file, "w");
  if(f == NULL)
  {
    exit(1);
  }

  struct stat info;
  struct stat fs;
  int r;
  int s;

  if(dp != NULL)
  {
    while((ep = readdir(dp)))
    {
      if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        char temp[999];
        char temp2[999];
        if(folder == "darat")
        {
          strcpy(temp, "darat/");
          strcpy(temp2, "darat/");
        }
        else
        {
          strcpy(temp, "air/");
          strcpy(temp2, "air/");
        }

        r = stat(strcat(temp, ep->d_name), &info);
        s = stat(strcat(temp2, ep->d_name), &fs);

        if(fs.st_mode & S_IRUSR)
        {
          fprintf(f, "r");
        }
        else
        {
          fprintf(f, "-");
        }

        if(fs.st_mode & S_IWUSR)
        {
          fprintf(f, "w");
        }
        else
        {
          fprintf(f, "-");
        }

        if(fs.st_mode & S_IXUSR)
        {
          fprintf(f, "x");
        }
        else
        {
          fprintf(f, "-");
        }

        char *fileName = ep->d_name;
        fprintf(f, "_%s \n", fileName);
      }
    }

    fclose(f);
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Fungsi tersebut untuk membuat list.txt, kemudian diisi dengan list hewan air.

#### 4. Main program
```bash
int main()
{
  pid_t child_id;
  child_id = fork();
  int status;

  if(child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if(child_id != 0)
  {
    createFolderDarat();
  }
  else
  {
    while(wait(&status) > 0);
    sleep(3);
    createFolderAir();
    unzipAnimalZip();
    listAnimals();
    removeBirdFromDarat();
    createTxt("darat/list.txt");
    writeTxt("darat", "darat/list.txt");
    createTxt("air/list.txt");
    writeTxt("air", "air/list.txt");
  }
}
```
Dari semua fungsi di atas, dimasukkan ke dalam main function dari program. Tapi sebelum itu, program membuat sebuah child process dan parent process lewat fork().

### Kendala dalam Pengerjaan
Saat pengerjaan soal 3, terdapat kendala pada proses pengextract-an animals.zip dimana proses extract terhenti sampai beberapa file saja padahal program sendiri masih berjalan di terminal. Kemudian, folder darat dan air tidak terbuat, malahan folder animals yang terbuat. Jadi, kode program yang dibuat masih memiliki error walaupun bisa dicompile dan dijalankan.

### Dokumentasi
![dokum_3.1](Dokumentasi/Soal3/animals.png)
